from contextlib import contextmanager
import errno
import fcntl
import hashlib
import os
import select
import shutil
import signal
import subprocess
import sys
import tempfile
import time
import unittest

import oelite.util
import oelite.signal
import oelite.compat
import oelite.lock

description = "Run tests of internal utility functions"
def add_parser_options(parser):
    parser.add_option("-s", "--show",
                      action="store_true", default=False,
                      help="Show list of tests")

class ExpectElapsedTime(object):
    """A helper to check elapsed time is mostly as expected."""
    def __init__(self, test_case, expected, delta=0.05):
        self.failureException = test_case.failureException
        self.expected = expected
        self.delta = delta
        self.start = time.time()
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_value, tb):
        elapsed = time.time() - self.start
        if abs(elapsed - self.expected) > self.delta:
            raise self.failureException(
                "elapsed time {0} differs from expected {1} by more than {2}".format(
                    elapsed, self.expected, self.delta
                )
            )
        return False

class OEliteTest(unittest.TestCase):
    def setUp(self):
        self.wd = tempfile.mkdtemp()
        os.chdir(self.wd)
        # Fork a child which will bring down the entire process group
        # if something takes too long.
        self.supervisor = os.fork()
        if self.supervisor == 0:
            time.sleep(15)
            print "Sorry, some test is taking too long"
            os.kill(-os.getppid(), signal.SIGKILL)
            # We've probably committed suicide, but just in case.
            os._exit(1)

    def tearDown(self):
        os.kill(self.supervisor, signal.SIGKILL)
        os.waitpid(self.supervisor, 0)
        os.chdir("/")
        shutil.rmtree(self.wd)

    def test_makedirs(self):
        """Test the semantics of oelite.util.makedirs"""

        makedirs = oelite.util.makedirs
        touch = oelite.util.touch
        self.assertIsNone(makedirs("x"))
        self.assertIsNone(makedirs("x"))
        self.assertIsNone(makedirs("y/"))
        self.assertIsNone(makedirs("y/"))
        self.assertIsNone(makedirs("x/y/z"))
        # One can create multiple leaf directories in one go; mkdir -p
        # behaves the same way.
        self.assertIsNone(makedirs("z/.././z//w//../v"))
        self.assertTrue(os.path.isdir("z/w"))
        self.assertTrue(os.path.isdir("z/v"))

        self.assertIsNone(touch("x/a"))
        with self.assertRaises(OSError) as cm:
            makedirs("x/a")
        self.assertEqual(cm.exception.errno, errno.ENOTDIR)
        with self.assertRaises(OSError) as cm:
            makedirs("x/a/z")
        self.assertEqual(cm.exception.errno, errno.ENOTDIR)

        self.assertIsNone(os.symlink("a", "x/b"))
        with self.assertRaises(OSError) as cm:
            makedirs("x/b")
        self.assertEqual(cm.exception.errno, errno.ENOTDIR)
        with self.assertRaises(OSError) as cm:
            makedirs("x/b/z")
        self.assertEqual(cm.exception.errno, errno.ENOTDIR)

        self.assertIsNone(os.symlink("../y", "x/c"))
        self.assertIsNone(makedirs("x/c"))
        self.assertIsNone(makedirs("x/c/"))

        self.assertIsNone(os.symlink("nowhere", "broken"))
        with self.assertRaises(OSError) as cm:
            makedirs("broken")
        self.assertEqual(cm.exception.errno, errno.ENOENT)

        self.assertIsNone(os.symlink("loop1", "loop2"))
        self.assertIsNone(os.symlink("loop2", "loop1"))
        with self.assertRaises(OSError) as cm:
            makedirs("loop1")
        self.assertEqual(cm.exception.errno, errno.ELOOP)

    def test_cloexec(self):
        open_cloexec = oelite.compat.open_cloexec
        dup_cloexec = oelite.compat.dup_cloexec

        def has_cloexec(fd):
            flags = fcntl.fcntl(fd, fcntl.F_GETFD)
            return (flags & fcntl.FD_CLOEXEC) != 0

        fd = open_cloexec("/dev/null", os.O_RDONLY)
        self.assertGreaterEqual(fd, 0)
        self.assertTrue(has_cloexec(fd))

        fd2 = os.dup(fd)
        self.assertGreaterEqual(fd2, 0)
        self.assertFalse(has_cloexec(fd2))
        self.assertIsNone(os.close(fd2))

        fd2 = dup_cloexec(fd)
        self.assertGreaterEqual(fd2, 0)
        self.assertTrue(has_cloexec(fd2))
        self.assertIsNone(os.close(fd2))

        self.assertIsNone(os.close(fd))

    def test_hash_file(self):
        testv = [(      0, "d41d8cd98f00b204e9800998ecf8427e", "da39a3ee5e6b4b0d3255bfef95601890afd80709"),
                 (      1, "0cc175b9c0f1b6a831c399e269772661", "86f7e437faa5a7fce15d1ddcb9eaeaea377667b8"),
                 (   1000, "cabe45dcc9ae5b66ba86600cca6b8ba8", "291e9a6c66994949b57ba5e650361e98fc36b1ba"),
                 ( 131072, "81615449a98aaaad8dc179b3bec87f38", "ce5653590804baa9369f72d483ed9eba72f04d29"),
                 (1000000, "7707d6ae4e027c70eea2a935c2296f21", "34aa973cd4c4daa4f61eeb2bdbad27316534016f")]
        hash_file = oelite.util.hash_file

        for size, md5, sha1 in testv:
            # open and say "aaaa...." :-)
            with tempfile.NamedTemporaryFile() as tmp:
                self.assertIsNone(tmp.write("a"*size))
                self.assertIsNone(tmp.flush())
                self.assertEqual(os.path.getsize(tmp.name), size)

                h = hash_file(hashlib.md5(), tmp.name).hexdigest()
                self.assertEqual(h, md5)

                h = hash_file(hashlib.sha1(), tmp.name).hexdigest()
                self.assertEqual(h, sha1)

    def test_callable_list(self):
        CL = oelite.util.CallableList
        def add(x, n):
            x[0] += n
        def mul(x, n):
            x[0] *= n
        x = [0]
        m = CL([add])
        m(x, 1)
        self.assertEqual(x[0], 1)
        m.append(mul)
        m(x, 2)
        self.assertEqual(x[0], 6)
        m += [add]
        m(x, 3)
        self.assertEqual(x[0], 30)

class MakedirsRaceTest(OEliteTest):
    def child(self):
        signal.alarm(2) # just in case of infinite recursion bugs
        try:
            # wait for go
            select.select([self.r], [], [], 1)
            oelite.util.makedirs(self.path)
            # no exception? all right
            res = "OK"
        except OSError as e:
            # errno.errorcode(errno.ENOENT) == "ENOENT" etc.
            res = errno.errorcode.get(e.errno) or str(e.errno)
        except Exception as e:
            res = "??"
        finally:
            # Short pipe writes are guaranteed atomic
            os.write(self.w, res+"\n")
            os._exit(0)

    def setUp(self):
        super(MakedirsRaceTest, self).setUp()
        self.path = "x/" * 10
        self.r, self.w = os.pipe()
        self.children = []
        for i in range(8):
            pid = os.fork()
            if pid == 0:
                self.child()
            self.children.append(pid)

    def runTest(self):
        """Test concurrent calls of oelite.util.makedirs"""

        os.write(self.w, "go go go\n")
        time.sleep(0.01)
        os.close(self.w)
        with os.fdopen(self.r) as f:
            v = [v.strip() for v in f]
        d = {x: v.count(x) for x in v if x != "go go go"}
        # On failure this won't give a very user-friendly error
        # message, but it should contain information about the errors
        # encountered.
        self.assertEqual(d, {"OK": len(self.children)})
        self.assertTrue(os.path.isdir(self.path))

    def tearDown(self):
        for pid in self.children:
            os.kill(pid, signal.SIGKILL)
            os.waitpid(pid, 0)
        super(MakedirsRaceTest, self).tearDown()

class SigPipeTest(OEliteTest):
    def run_sub(self, preexec_fn):
        from subprocess import PIPE, Popen

        sub = Popen(["yes"], stdout=PIPE, stderr=PIPE,
                    preexec_fn = preexec_fn)
        # Force a broken pipe.
        sub.stdout.close()
        err = sub.stderr.read()
        ret = sub.wait()
        return (ret, err)

    @unittest.skipIf(sys.version_info >= (3, 2), "Python is new enough")
    def test_no_restore(self):
        """Check that subprocesses inherit the SIG_IGNORE disposition for SIGPIPE."""
        (ret, err) = self.run_sub(None)
        # This should terminate with a write error; we assume that
        # 'yes' is so well-behaved that it both exits with a non-zero
        # exit code as well as prints an error message containing
        # strerror(errno).
        self.assertGreater(ret, 0)
        self.assertIn(os.strerror(errno.EPIPE), err)

    def test_restore(self):
        """Check that oelite.signal.restore_defaults resets the SIGPIPE disposition."""
        (ret, err) = self.run_sub(oelite.signal.restore_defaults)
        # This should terminate due to SIGPIPE, and not get a chance
        # to write to stderr.
        self.assertEqual(ret, -signal.SIGPIPE)
        self.assertEqual(err, "")

class FileLockTest(OEliteTest):
    def test_basic(self):
        """Test the basic FileLock interface"""
        lf = oelite.lock.FileLock("foo")

        self.assertTrue(lf.lock(shared = False))
        self.assertTrue(os.path.exists("foo"))
        self.assertIsNone(lf.unlock())
        self.assertFalse(os.path.exists("foo"))

        self.assertTrue(lf.lock(shared = True))
        self.assertTrue(os.path.exists("foo"))
        self.assertIsNone(lf.unlock())
        self.assertFalse(os.path.exists("foo"))

    def test_multiple(self):
        """Test the behaviour when multiple FileLocks are in play """
        lf1 = oelite.lock.FileLock("foo")
        lf2 = oelite.lock.FileLock("foo")

        self.assertFalse(os.path.exists("foo"))

        # With one LOCK_EX, both LOCK_SH and LOCK_EX should timeout.
        self.assertTrue(lf1.lock(shared = False))
        self.assertTrue(os.path.exists("foo"))

        with ExpectElapsedTime(self, 0):
            self.assertFalse(lf2.lock(shared = False, timeout = 0))
        self.assertTrue(os.path.exists("foo"))

        with ExpectElapsedTime(self, .3):
            self.assertFalse(lf2.lock(shared = False, timeout = .3))
        self.assertTrue(os.path.exists("foo"))

        with ExpectElapsedTime(self, .4):
            self.assertFalse(lf2.lock(shared = True, timeout = .4))
        self.assertTrue(os.path.exists("foo"))

        self.assertIsNone(lf1.unlock())
        self.assertFalse(os.path.exists("foo"))

        # With LOCK_SH, LOCK_SH should succeed (immediately) while LOCK_EX should time out
        self.assertTrue(lf1.lock(shared = True))
        self.assertTrue(os.path.exists("foo"))

        with ExpectElapsedTime(self, 0):
            self.assertTrue(lf2.lock(shared = True, timeout = .4))
        self.assertTrue(os.path.exists("foo"))
        self.assertIsNone(lf2.unlock())
        self.assertTrue(os.path.exists("foo"))

        with ExpectElapsedTime(self, .3):
            self.assertFalse(lf2.lock(shared = False, timeout = .3))
        self.assertTrue(os.path.exists("foo"))

        self.assertIsNone(lf1.unlock())
        self.assertFalse(os.path.exists("foo"))

        # Also test that if lf2 is the last to unlock it still gets cleaned up.
        self.assertTrue(lf1.lock(shared = True))
        self.assertTrue(os.path.exists("foo"))
        self.assertTrue(lf2.lock(shared = True))
        self.assertIsNone(lf1.unlock())
        self.assertTrue(os.path.exists("foo"))
        self.assertIsNone(lf2.unlock())
        self.assertFalse(os.path.exists("foo"))

    @contextmanager
    def flock_sleep(self, shared, delay):
        """Run 'flock -n <-x|-s> foo sleep <delay> in a subprocess"""
        cmd = ["flock", "-n"]
        cmd.append(("-x", "-s")[shared])
        cmd.append("foo")
        cmd.append("sleep")
        cmd.append(str(delay))
        sub = subprocess.Popen(cmd)
        # If we're using flock(1) to hold a lock for a while, we must
        # ensure that the subprocess has actually proceeded to taking
        # that lock. So sleep for a very short while before returning.
        time.sleep(0.01)
        self.assertTrue(os.path.exists("foo"))
        yield sub
        sub.wait()

    def test_external(self):
        """Run various tests where an external process is involved."""
        lf = oelite.lock.FileLock("foo")

        # Let flock(1) hold an exclusive lock.
        self.assertFalse(os.path.exists("foo"))
        with self.flock_sleep(False, 1.0) as sub:
            with ExpectElapsedTime(self, .4):
                self.assertFalse(lf.lock(shared = False, timeout = .4))
            # The lock should become available after ~ .6 seconds more
            with ExpectElapsedTime(self, .6):
                self.assertTrue(lf.lock(shared = False, timeout = .7))
        self.assertEqual(sub.returncode, 0)
        self.assertTrue(os.path.exists("foo"))
        self.assertIsNone(lf.unlock())

        # Let flock(1) hold a shared lock.
        self.assertFalse(os.path.exists("foo"))
        with self.flock_sleep(True, 0.5) as sub:
            with ExpectElapsedTime(self, .3):
                self.assertFalse(lf.lock(shared = False, timeout = .3))
            with ExpectElapsedTime(self, 0):
                self.assertTrue(lf.lock(shared = True, timeout = .4))
            self.assertIsNone(lf.unlock())
            self.assertTrue(os.path.exists("foo"))
            # Check that a 'flock -n -x' call will fail
            with self.flock_sleep(False, 1.0) as sub2:
                pass
            # The return code of 1 is documented in 'man 1 flock'.
            self.assertEqual(sub2.returncode, 1)
            # Take a shared lock again
            with ExpectElapsedTime(self, 0):
                self.assertTrue(lf.lock(shared = True, timeout = .4))

        self.assertEqual(sub.returncode, 0)
        self.assertTrue(os.path.exists("foo"))
        self.assertIsNone(lf.unlock())
        self.assertFalse(os.path.exists("foo"))

def run(options, args, config):
    os.setpgid(0, 0)
    suite = unittest.TestSuite()
    suite.addTest(MakedirsRaceTest())
    suite.addTest(OEliteTest('test_makedirs'))
    suite.addTest(SigPipeTest('test_no_restore'))
    suite.addTest(SigPipeTest('test_restore'))
    suite.addTest(OEliteTest('test_cloexec'))
    suite.addTest(OEliteTest('test_hash_file'))
    suite.addTest(FileLockTest('test_basic'))
    suite.addTest(FileLockTest('test_multiple'))
    suite.addTest(FileLockTest('test_external'))
    suite.addTest(OEliteTest('test_callable_list'))

    if options.show:
        for t in suite:
            print str(t), "--", t.shortDescription()
        return 0
    runner = unittest.TextTestRunner(verbosity=3)
    runner.run(suite)

    return 0
