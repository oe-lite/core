import oebakery
from oebakery import die, err, warn, info, debug
from oelite import *
from recipe import OEliteRecipe
from runq import OEliteRunQueue
import oelite.meta
import oelite.util
import oelite.arch
import oelite.parse
import oelite.task
import oelite.item
from oelite.parse import *
from oelite.cookbook import CookBook
from oelite.compat import open_cloexec

import oelite.fetch

import bb.utils

import sys
import os
import glob
import shutil
import hashlib
import logging
import time
import errno
import select

class OEliteOven:
    def __init__(self, baker, capacity=None):
        if capacity is None:
            pmake = baker.config.get("PARALLEL_MAKE")
            if pmake is None or pmake == "":
                capacity = 2
            else:
                capacity = int(pmake.replace("-j", ""))
        self.setup_fifo(baker.config.get("TMPDIR"), capacity)
        self.capacity = capacity
        self.baker = baker
        self.starttime = dict()
        self.completed_tasks = []
        self.failed_tasks = []
        self.total = baker.runq.number_of_tasks_to_build()
        self.count = 0
        self.task_stat = dict()
        self.stdout_isatty = os.isatty(sys.stdout.fileno())

    def setup_fifo(self, tmpdir, capacity):
        self.fifo_path = os.path.join(tmpdir, "jobserver.fifo")
        try:
            os.mkfifo(self.fifo_path, 0666)
        except OSError as e:
            if e.errno == errno.EEXIST:
                pass
            else:
                raise
        self.fifo_rfd = open_cloexec(self.fifo_path, os.O_RDONLY | os.O_NONBLOCK)
        self.fifo_wfd = open_cloexec(self.fifo_path, os.O_WRONLY)
        # Drain the fifo in case there are leftover tokens
        while True:
            try:
                os.read(self.fifo_rfd, 128)
            except OSError as e:
                if e.errno == errno.EAGAIN:
                    break
                raise
        os.write(self.fifo_wfd, "+" * (capacity - 1))
        self.prepped_token = False

    def read_token(self):
        assert(not self.prepped_token)
        try:
            token = os.read(self.fifo_rfd, 1)
        except OSError as e:
            if e.errno == errno.EAGAIN:
                return False
            raise
        self.prepped_token = True
        return True

    def prepare_token(self):
        assert(not self.prepped_token)
        # We're always allowed to start one job.
        if len(self) == 0:
            self.prepped_token = True
            return True
        if self.read_token():
            return True
        # If the oven itself currently uses more than half the tokens
        # - in particular, if all tokens are currently used for
        # starting oven jobs - just give up and let the caller wait
        # for one of the current jobs to finish. Otherwise, let's wait
        # a small while to see if a token becomes available. Of
        # course, even if poll() says the fd is readable, someone else
        # might snatch the token before we get to do the read().
        if len(self) >= self.capacity/2:
            return False
        select.select([self.fifo_rfd], [], [], 1.0)
        return self.read_token()

    # The tasks which are currently baking are the keys in the
    # .starttime member. Implementing __contains__ makes sense of
    # "task in oven".
    def __contains__(self, x):
        return x in self.starttime

    # Allow "for t in oven:"
    def __iter__(self):
        return iter(self.starttime)

    # Size of oven == number of currently baking tasks.
    def __len__(self):
        return len(self.starttime)

    def currently_baking(self):
        return list(self)

    def update_task_stat(self, task, delta):
        try:
            stat = self.task_stat[task.name]
        except KeyError:
            stat = self.task_stat[task.name] = oelite.profiling.SimpleStats()
        stat.append(delta)

    def add(self, task):
        self.starttime[task] = oelite.util.now()
        assert(self.prepped_token)
        self.prepped_token = False

    def remove(self, task):
        now = oelite.util.now()
        delta = now - self.starttime[task]
        del self.starttime[task]
        self.update_task_stat(task, delta)
        # Some task in the oven is using our "special" token. We don't
        # need to keep track of which one: If the just finished task
        # was started using that, any other task in the oven must have
        # been started with a token from the pipe. So we can imagine
        # transferring our special token to that still running task,
        # while writing back that task's token to the pipe. Of course,
        # if this was the last task in the oven, we must not write a
        # token to the pipe.
        if len(self) > 0:
            os.write(self.fifo_wfd, "+")
        return delta

    def start(self, task):
        self.count += 1
        debug("")
        debug("Preparing %s"%(task))
        task.prepare()
        info("%s started - %d / %d "%(task, self.count, self.total))
        task.build_started()

        self.add(task)
        task.start()

    def wait_task(self, poll, task):
        """Wait for a specific task to finish baking. Returns pair (result,
        delta), or None in case poll=True and the task is not yet
        done.

        """
        assert(task in self)
        result = task.wait(poll)
        if result is None:
            return None
        delta = self.remove(task)
        task.task_time = delta

        task.recipe.remaining_tasks -= 1
        if result:
            info("%s finished - %.3f s" % (task, delta))
            task.build_done(self.baker.runq.get_task_buildhash(task))
            self.baker.runq.mark_done(task)
            self.completed_tasks.append(task)
        else:
            err("%s failed - %.3f s" % (task, delta))
            self.failed_tasks.append(task)
            task.build_failed()
            # If any task for a recipe fails, ensure that we don't do rmwork.
            task.recipe.rmwork = False

        if task.recipe.remaining_tasks == 0:
            task.recipe.do_rmwork()

        return (task, result, delta)

    def wait_any(self, poll):
        """Wait for any task currently in the oven to finish. Returns triple
        (task, result, time), or None.

        """
        if not poll and len(self) == 0:
            raise Exception("nothing in the oven, so you'd wait forever...")
        tasks = self.currently_baking()
        if not poll and len(tasks) == 1:
            t = tasks[0]
            if self.stdout_isatty:
                now = oelite.util.now()
                info("waiting for %s (started %.3f seconds ago) to finish" % (t, now-self.starttime[t]))
            return self.wait_task(False, t)
        tasks.sort(key=lambda t: self.starttime[t])
        i = 0
        while True:
            for t in tasks:
                result = self.wait_task(True, t)
                if result is not None:
                    return result
            if poll:
                break
            i += 1
            if i == 4 and self.stdout_isatty:
                info("waiting for any of these to finish:")
                now = oelite.util.now()
                for t in tasks:
                    info("  %-40s started %.3f seconds ago" % (t, now-self.starttime[t]))
            time.sleep(0.1)
        return None

    def wait_all(self, poll):
        """Do wait_task once for every task currently in the oven once. With
        poll=False, this amounts to waiting for every current task to
        finish.

        """
        tasks = self.currently_baking()
        tasks.sort(key=lambda t: self.starttime[t])
        for t in tasks:
            self.wait_task(poll, t)

    def write_profiling_data(self):
        with oelite.profiling.profile_output("task_stat.txt") as out:
            for name,stats in self.task_stat.iteritems():
                stats.compute()
                quarts = ", ".join(["%7.3f" % x for x in stats.quartiles])
                out.write("%-16s  %7.1fs / %5d = %7.3fs  [%s]\n" %
                          (name, stats.sum, stats.count, stats.mean, quarts))

        with oelite.profiling.profile_output("task_times.txt") as f:
            for task in self.completed_tasks:
                f.write("%s\t%.3f\t%.3f\t%.3f\t%.3f\n" %
                        (task, task.task_time, task.prefunc_time, task.func_time, task.postfunc_time))

